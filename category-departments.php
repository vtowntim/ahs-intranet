<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package ahs
 */

get_header(); ?>

<?php
	if (is_category( )) {
		$cat = get_query_var('cat');
		$currentCat = get_category ($cat);
	}
?>

<div class="container">
	<div class="row">
		<div class="landing col-sm-8">

			<h1><?php single_cat_title(); ?></h1>


			<?php
				$catLandingArgs = array(
					'category_name'	=> $currentCat->slug,
					'tag'			=> 'landing',
				);
				$catLandings = new WP_Query($catLandingArgs); while($catLandings->have_posts()) : $catLandings->the_post(); ?>

					<h2 class="landing-page-title"><?php the_title(); ?></h2>
					<?php the_content(); ?>
					<?php edit_post_link( __( 'Edit', 'ahs' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>

			<?php endwhile; wp_reset_postdata(); ?>


			<h2>News</h2>

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/* Include the Post-Format-specific template for the content.
						 * If you want to overload this in a child theme then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );
					?>

				<?php endwhile; wp_reset_postdata(); ?>


			<?php else : ?>

				<?php //get_template_part( 'no-results', 'index' ); ?>

			<?php endif; ?>

		</div>

		<div class="department-pages col-sm-4">
			<h2>Pages</h2>
			<?php

				$catPagesQuery = "
					SELECT $wpdb->posts.*
					FROM $wpdb->posts, $wpdb->terms, $wpdb->term_relationships
					WHERE $wpdb->terms.term_id = $wpdb->term_relationships.term_taxonomy_id
					AND $wpdb->posts.ID = $wpdb->term_relationships.object_id
					AND $wpdb->terms.slug = '$currentCat->slug'
					AND $wpdb->posts.post_status = 'publish'
					AND $wpdb->posts.post_type = 'page'
					AND $wpdb->posts.post_date < NOW()
					ORDER BY $wpdb->posts.post_title ASC
				";

				$catPages = $wpdb->get_results($catPagesQuery, OBJECT); ?>

				<?php if ($catPages): ?>
					<?php global $post; ?>
					<?php foreach ($catPages as $post): ?>
						<?php setup_postdata($post); ?>
						<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo the_title(); ?></a></li>
					<?php endforeach; ?>
				<?php endif; ?>




		</div>
	</div>
</div>

<?php /* get_sidebar(); */ ?>
<?php get_footer(); ?>