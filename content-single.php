<?php
/**
 * @package ahs
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">
		<h1 class="page-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php ahs_posted_on(); ?>
		</div><!-- .entry-meta -->

		<div class="post-thumbnail">
			<?php if ( has_post_thumbnail( $thumbnail->ID) ) : ?>

				<?= get_the_post_thumbnail( $thumbnail->ID, 'large', array(
					'class' => 'img-responsive',
				) ); ?>

			<?php endif; ?>
		</div>

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'ahs' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
