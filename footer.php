<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package ahs
 */
?>

</div><!-- close .main-content -->

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="row">

			<?php wp_nav_menu(
				array(
					'theme_location' => 'footerMenu',
					'container_class' => 'col-sm-10',
					'menu_class' => 'nav nav-pills',
					'fallback_cb' => '',
					'menu_id' => 'footerMenu',
					'walker' => new wp_bootstrap_navwalker()
				)
			); ?>

			<div class="col-sm-1">
				<a href="<?= wp_login_url(); ?>" id="adminLink" class="btn">❖</a>
			</div>

		</div>
		<div class="row">
			<div class="helpdesk-info col-sm-12">

				<h5>AHS Help Desk:</h5> 
				<a class="btn" href="http://remcluweb04.resdm50.siemensmedasp.com/emfgw/gw.asp?sitevar=ac14">Self-Help (GEMS)</a>
				<label>Main Number:</label> 
				<span class='telephone-number'>437-4503 <span class='telephone-extension'>(EXT. 44503)</span></span>

			</div>
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->

<?php wp_footer(); ?>

</body>
</html>