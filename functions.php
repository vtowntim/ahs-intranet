<?php
/**
 * ahs functions and definitions
 *
 * @package ahs
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 750; /* pixels */

if ( ! function_exists( 'ahs_setup' ) ) :
/**
 * Set up theme defaults and register support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function ahs_setup() {
	global $cap, $content_width;

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	if ( function_exists( 'add_theme_support' ) ) {

		/**
		 * Add default posts and comments RSS feed links to head
			add_theme_support( 'automatic-feed-links' );
		*/
		

		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		add_theme_support( 'post-thumbnails' );

		/**
		 * Enable support for Post Formats
		*/
		add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

		/**
		 * Setup the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'ahs_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
		*/

	}

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on ahs, use a find and replace
	 * to change 'ahs' to the name of your theme in all the template files
	load_theme_textdomain( 'ahs', get_template_directory() . '/languages' );
	*/
	

	/**
	 * This theme uses wp_nav_menu() in one location.
	*/
	register_nav_menus( array(
		'headerMenu'  => __( 'Header Menu', 'ahs' ),
		'quickLinks'  => __( 'Quick Links', 'ahs' ),
		'departments'  => __( 'Departments', 'ahs' ),
		'web-applications'  => __( 'Web Applications', 'ahs' ),
		'footerMenu'  => __( 'Footer Menu', 'ahs' ),
	) );

}
endif; // ahs_setup
add_action( 'after_setup_theme', 'ahs_setup' );


/**
 * Iterate up current category hierarchy until a template is found.
 * 
 * @link http://stackoverflow.com/a/3120150/247223
 */ 
function so_3119961_load_cat_parent_template() {
    global $wp_query;

    if ( ! $wp_query->is_category )
        return true;

    // Get current category object
    $term = $wp_query->get_queried_object();

    // Iterate up the hierarchy and locate a template
    while ( $term && ! is_wp_error( $term ) ) {
        if ( locate_template( "category-{$term->slug}.php", true, false ) )
            exit;

        $term = $term->parent ? get_category( $term->parent ) : false;
    }
}

add_action( 'template_redirect', 'so_3119961_load_cat_parent_template' );



/**
 * Register widgetized area and update sidebar with default widgets
 */
function ahs_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'ahs' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Home-Right', 'ahs' ),
		'id'            => 'home-right-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'ahs_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function ahs_scripts() {

	// load bootstrap css
	wp_enqueue_style( 'ahs-bootstrap', get_template_directory_uri() . '/includes/resources/bootstrap/css/bootstrap.min.css' );

	// load Font Awesome css
	wp_enqueue_style( 'ahs-font-awesome', get_template_directory_uri() . '/includes/css/font-awesome.min.css', false, '4.1.0' );

	// load ahs styles
	wp_enqueue_style( 'ahs-style', get_stylesheet_uri() );

	// load bootstrap js
	wp_enqueue_script('ahs-bootstrapjs', get_template_directory_uri().'/includes/resources/bootstrap/js/bootstrap.min.js', array('jquery') );

	// load bootstrap wp js
	wp_enqueue_script( 'ahs-bootstrapwp', get_template_directory_uri() . '/includes/js/bootstrap-wp.js', array('jquery') );

	// load sugar.js
	wp_enqueue_script( 'sugar-js', get_template_directory_uri() . '/includes/js/sugar.min.js', false, null, true );

	wp_enqueue_script( 'ahs-skip-link-focus-fix', get_template_directory_uri() . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'ahs-keyboard-image-navigation', get_template_directory_uri() . '/includes/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

	// load ahsIntranet script
	wp_enqueue_script( 'ahs-intranet', get_template_directory_uri() . '/includes/js/ahsIntranet.js', false, null, true );


}
add_action( 'wp_enqueue_scripts', 'ahs_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/includes/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/includes/bootstrap-wp-navwalker.php';
