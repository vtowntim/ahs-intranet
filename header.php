<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package ahs
 */
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="<?= get_template_directory_uri(); ?>/includes/js/html5shiv.min.js"></script>
		<script src="<?= get_template_directory_uri(); ?>/includes/js/respond.min.js"></script>
	<![endif]-->

</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>


<header id="masthead" class="site-header" role="banner">
	<div class="container-fluid">
		<div class="row">
			<div class="site-header-inner site-header-inner-left col-sm-3">
				<div class="site-branding">
					<a id="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src='<?php bloginfo( 'template_url' ); ?>/includes/images/AHS-logo.png' />
					</a>
				<!--
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h4 class="site-description"><?php bloginfo( 'description' ); ?></h4>
				-->
				</div>

			</div>

			<div class="site-header-inner hidden-xs site-header-inner-middle col-sm-6">
			</div>

			<div class="site-header-inner hidden-xs site-header-inner-right col-sm-3">
			</div>



		</div>
	</div><!-- .container -->
</header><!-- #masthead -->



<nav class="site-navigation navbar navbar-default navbar-inverse" role="navigation">
	<div class="container-fluid">

		<div class="navbar-header">
			<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<!-- Your site title as branding in the menu -->
			<?php /* <!-- <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a> -->  */?>
		</div>

		<!-- The WordPress Menu goes here -->
		<?php wp_nav_menu(
			array(
				'theme_location' => 'headerMenu',
				'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
				'menu_class' => 'nav navbar-nav',
				'fallback_cb' => '',
				'menu_id' => 'main-menu',
				'walker' => new wp_bootstrap_navwalker()
			)
		); ?>

	</div><!-- .container -->
</nav><!-- .site-navigation -->

<div class="main-content">

<?php get_template_part( 'modals' ); ?>
