$ = jQuery

AHS = {}


AHS.featuredCarousel = ()->
	$("#featuredCarousel .item").each (_index, _item) ->
		$_newIndicator = $("<li data-target=\"#featuredCarousel\" data-slide-to=\"" + _index + "\"></li>")
		$_newIndicator.addClass "active"  if _index is 0
		$_newIndicator.appendTo "#featuredCarousel .carousel-indicators"
		return
	$(".carousel .item").eq(0).addClass "active"
	$(".carousel").carousel interval: 6 * 1000 #1000 milliseconds = 1s
	return





AHS.evenColumns = (_sourceULContainer, _columns) ->
	$_lis = $(_sourceULContainer).find("li")
	_remainders = $_lis.length % _columns
	__count = 0
	$_evenColumns = $(_sourceULContainer).find('.evenColumn')
	while __count++ isnt _columns
		_addRemainder = ((if _remainders-- > 0 then 1 else 0))
		_itemsToGrab = Math.floor($_lis.length / _columns) + _addRemainder
		if __count > 1
			$_evenColumns.eq(0).find('li').slice(_firstColumnLength, _itemsToGrab + _firstColumnLength).remove().appendTo $(_sourceULContainer).find('.evenColumn').eq(__count - 1)
		else
			_firstColumnLength = _itemsToGrab
	return

AHS.buildModals_wrapSubMenus = (_AHSModal)->
	$(_AHSModal).find('.AHSModal_Source').removeClass('AHSModal_Source')
	$(_AHSModal).find('.sub-menu').wrap("<div class='AHSModal_Source'></div>")
	return

AHS.buildModals = ()->
	$('.AHSModal').each (_index, _AHSModal)->
		_depth = $(_AHSModal).data('depth')
		_slug = $(_AHSModal).data('slug')
		_title = $(_AHSModal).data('title')
		_columns = $(_AHSModal).data('columns')
		_columnWidth = Math.floor(12 / _columns)

		if _depth is 2
			AHS.buildModals_wrapSubMenus _AHSModal

		$(_AHSModal).find('.AHSModal_Source ul').addClass("col-sm-#{_columnWidth}").addClass('evenColumn')

		$(_AHSModal).find('.AHSModal_Source').each (_sourceIndex, _sourceULContainer)->
			# $(_sourceULContainer).append()
			__randomID = Math.floor(Math.random()*100)
			__count = 1
			while __count++ isnt _columns
				$("<ul class='evenColumn col-sm-#{_columnWidth}'></ul>").appendTo(_sourceULContainer)

			AHS.evenColumns _sourceULContainer, _columns

			$(_sourceULContainer).parent('li').addClass('col-sm-12').wrap('<div class="row"></div>')

			return

		# $("<li><a class='navbar-nav' data-toggle='modal' data-target='##{_slug}'>#{_title}</a></li>").insertAfter $('#main-menu li:has(a[title=Home])')
		return

	$prevAnchorTag = $('.sub-menu').parent('.AHSModal_Source').prev('a')

	$prevAnchorTag.each (prevAnchorIndex, prevAnchor)->
		$prevAnchorTagText = $(prevAnchor).text()
		$(prevAnchor).replaceWith "<h3 class='SubMenuTitle'>#{$prevAnchorTagText}</h3>"

	.addClass('SubMenuTitle').removeAttr('href')
	return


AHS.buildModalLinks = ()->
	$('#main-menu a[href^="#modal"]').each (modalIndex,modal)->
		$modal = $(modal)
		_mySlug = $(modal).attr('href').replace(/#modal-/,'')
		$modal.attr("data-target", "##{_mySlug}")
		$modal.attr("data-toggle", "modal")
		$modal.removeAttr("href")
		return
	return



## Running when the page loads:
$ ()->
	# Create the Quick Links Menu Modal:
	AHS.buildModals()

	# Create Links to Modals
	AHS.buildModalLinks()


	# If we're on the front page, activate the carousel:
	if $("#featuredCarousel").length
		AHS.featuredCarousel()

	return

