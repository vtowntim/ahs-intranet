<?php
/**
 * Modals
 *
 *
 */
?>


<?php

	function AHS_Modal($slug, $Title, $columns, $depth){
		$columnWidth = floor(12 / $columns);

		echo "<div id='" . $slug . "' class='modal AHSModal fade' data-depth='" . $depth . "' data-slug='" . $slug . "' data-columns='" . $columns . "' data-title='" . $Title . "' tabindex='-1' role='dialog'>
			<div class='modal-dialog modal-lg'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
						<h4 class='modal-title'>" . $Title . "</h4>
					</div>
					<div class='modal-body'>
						<div class='row'>
							<div class='AHSModal_Source col-sm-12'>
							";

								wp_nav_menu(
									array(
										'theme_location' 	=> $slug,
										'container_class' 	=> 'row',
										'container'		=> '',
										'menu_class' 		=> '',
										'fallback_cb' 		=> '',
										'menu_id' 		=> $slug . '-menu',
										'depth'			=> $depth
									)
								);

		echo"				</div>
						</div>
					</div>
				</div>
			</div>
		</div>";

	};

?>



<?php


AHS_Modal('quickLinks', 'Quick Links', 3, 1);
AHS_Modal('departments', 'Departments', 3, 1);
AHS_Modal('web-applications', 'Web Applications', 3, 2);

?>

<!-- Ending modals.php -->