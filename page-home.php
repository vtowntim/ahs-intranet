<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package ahs
 */

get_header(); ?>

<div class="container">
	<div class="row">
		<div class="home-top-left col-sm-8">

			<div id="featuredCarousel" class="carousel slide">

				<!-- Indicators -->
				<ol class="carousel-indicators">
				</ol>

				<!-- Slides -->
				<div class="carousel-inner">
					<?php
						$query1 = new WP_Query( 'tag=featured&posts_per_page=10' );
						// The Loop
						while ( $query1->have_posts() ) : $query1->the_post();

							if ( has_post_thumbnail( $thumbnail->ID) ) :
							?>
								<div class="item">
									<a href="<?= get_permalink( $thumbnail->ID ); ?>" title="<?= esc_attr( $thumbnail->post_title ); ?>">
										<?= get_the_post_thumbnail( $thumbnail->ID ); ?>
										<div class="carousel-caption">
											<h2><?= the_title('','',false); ?></h2>
										</div>
									</a>
								</div>

							<?php
							endif;
						endwhile;
					?>
				</div>
				<a class="left carousel-control" href="#featuredCarousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#featuredCarousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>

			</div>


		</div>

		<div class="home-top-right col-sm-4">
			<?php if ( is_active_sidebar( 'home-right-1' ) ) : ?>
				<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'home-right-1' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div id="content" class="main-content-inner col-md-12">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'home' ); ?>

			<?php endwhile; // end of the loop. ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>
