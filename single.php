<?php
/**
 * The Template for displaying all single posts.
 *
 * @package ahs
 */

get_header(); ?>

<div class="container">
	<div class="row">
		<div class="single-article col-sm-9">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>

				<?php ahs_content_nav( 'nav-below' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					// if ( comments_open() || '0' != get_comments_number() )
						// comments_template();
				?>

			<?php endwhile; // end of the loop. ?>

		</div>

		<div id="sidebar" class="col-sm-3">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>


<?php get_footer(); ?>